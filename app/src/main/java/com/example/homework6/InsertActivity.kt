package com.example.homework6

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.room.Room
import kotlinx.android.synthetic.main.activity_insert.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch

class InsertActivity : AppCompatActivity() {

    private val db: AppDatabase by lazy {
        Room.databaseBuilder(
            applicationContext,
            AppDatabase::class.java, "database-name"
        ).build()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_insert)

        init()
    }

    private fun init() {
        saveButton.setOnClickListener {
            CoroutineScope(IO).launch {
                insert()
                openMainActivity()
            }
        }
    }

    private fun openMainActivity() {
        var intent = Intent(this, MainActivity :: class.java)
        startActivity(intent)
    }


    private suspend fun insert() {
        val p = Post()
        p.title = titleEditText.text.toString()
        p.description = descriptionEditText.text.toString()
        db.userDao().insertAll(p)
        Log.d("my", "INSERT")
    }
}