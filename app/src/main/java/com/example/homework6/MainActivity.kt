package com.example.homework6

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.room.Room
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.launch

class MainActivity : AppCompatActivity() {

    val db: AppDatabase by lazy {
        Room.databaseBuilder(
                applicationContext,
                AppDatabase::class.java, "database-name"
        ).build()
    }


    private lateinit var adapter: RecyclerViewAdapter
    private lateinit var pAdapt:PostInfo

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        init()
    }

    private fun init() {
        getList()

        insertButton.setOnClickListener {
            openInsertActivity()
        }
    }

    public fun openMainActivity() {
        var intent = Intent(this, MainActivity :: class.java)
        startActivity(intent)
    }

    private fun openInsertActivity() {
        var intent = Intent(this, InsertActivity :: class.java)
        startActivity(intent)
    }

    public fun openPostInfo(data: Post) {

        var intent = Intent(this, PostInfo(data) :: class.java)
        startActivity(intent)
    }

    private fun setView(info : List<Post>) {
        CoroutineScope(Main).launch {
            var list = info.toMutableList()

            setRecView(list)
        }
    }

    private fun setRecView(list : MutableList<Post>) {
        recyclerview.layoutManager = LinearLayoutManager(MainActivity())
        adapter = RecyclerViewAdapter(list, MainActivity(), db)
        recyclerview.adapter = adapter
    }

    private fun getList() {
        var list : List<Post> = listOf()
        CoroutineScope(IO).launch {
            var info = db.userDao().getAll()
            setView(info)
            list.forEach { Log.d("my", it.title) }
        }
    }
}
