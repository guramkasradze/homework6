package com.example.homework6

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_recyclerview_layout.view.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class RecyclerViewAdapter(private val items: MutableList<Post>, private val activity: MainActivity, private val db : AppDatabase): RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_recyclerview_layout, parent, false))

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()


    }



    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private lateinit var model: Post
        fun onBind() {
            model = items[adapterPosition]
            itemView.titleTextView.text = "#" + model.pid + " " + model.title
            itemView.descriptionTextView.text = model.description
            Log.d("myt", model.title)

            itemView.setOnClickListener {

            }

            itemView.edit_btn.setOnClickListener {
                activity.openPostInfo(model)
                Log.d("my", "NOPE")
            }

            itemView.delete_btn.setOnClickListener {
                CoroutineScope(Dispatchers.IO).launch {
                    db.userDao().delete(model)
                }
                activity.openMainActivity()
            }
        }
    }
}